#ifndef ANG_String_H
#define ANG_String_H

#include <ANG/Array.hpp>

namespace ANG{

class String: public ANG::ArrayBase< char>{
private:
	inline int count(const char *str, const char endChar = '\0') const;
public:
	String();
	String(const char c);
	String(const char *src);
	String(const String &src);
	String &operator+=(const char c);
	String &operator+=(const char *str);
	String &operator+=(const String &other);
	String operator+(const char c) const;
	String operator+(const char *str) const;
	String operator+(const String &other) const;
	bool operator==(const char c) const;
	bool operator==(const char *str) const;
	bool operator==(const String &other) const;
	bool isEmpty() const;
	int to_i() const;
	ANG::Array<ANG::String> split(const ANG::String &endChars = String(" \t\n\r")) const;
};

};// namespace ANG

#endif//ANG_String_H
