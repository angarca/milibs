#ifndef ANG_Matrix_HPP
#define ANG_Matrix_HPP

#include <ANG/Array.hpp>

namespace ANG{

template< class T, int IncW= 30, int IncH = 30>
class MatrixBase{
protected:
	Array< ArrayBase< T, IncH>, IncW> matrix;
public:
	MatrixBase( int W = 0, int H = 0, int StartW = IncW, int StartH = IncH): matrix( W, StartW){
		for( int i=0; i < W; i++){
			if( H < StartH)	matrix[i].setLength(StartH).cutLength();
			matrix[i].setLength(H);
		}
	}
	MatrixBase( const T **src, int srcW, int srcH): matrix( srcW, srcW){
		for( int i=0; i < srcW; i++){
			matrix[i].setLength( srcH).cutLength();
			for( int j=0; j < srcH; j++)	matrix[i][j] = src[i][j];
		}
	}
	MatrixBase( const MatrixBase<T,IncW,IncH> &src){
		this->matrix = src.matrix;
	}
	ArrayBase<T,IncH> &operator[](const int index) const {	return this->matrix[index];}
	ArrayBase<T,IncH> &operator()(const int index) const { return this->matrix(index);}
	Array< ArrayBase< T, IncH>, IncW> &operator()() const { return this->matrix;}
	MatrixBase &setDim( const int newW, const int newH){
		this->matrix.setLength( newW);
		for( int i=0; i < newW; i++)	this->matrix[i].setLength( newH);
	}
	int getW() const { return this->matrix.getLength();}
	int getH( const int index) const { return this->matrix[index].getLength();}
	int getH(){
		int i, m = 0, w = this->matrix.getLength();
		if( w != 0){
			m = this->matrix[0].getLength();
			for( i=1; i < w; i++) if( m < this->matrix[i].getLength()) m = this->matrix[i].getLength();
			for( i=0; i < w; i++) this->matrix[i].setLength( m);	// normalizeH
		}
		return m;
	}
	bool contains( const T &elem) const{
		bool contain = false;
		for( int i=0; i < this->matrix.getLength(); i++) contain = contain || this->matrix[i].contains( elem);
		return contain;
	}
	void find( const T &target, int &x, int &y) const{	// when Array::find replace -1 for throw adapt with catch instead of if
		y = -1;
		for( x=0; y == -1 && x < this->matrix.getLength(); x++) y = this->matrix[x].find( target);
		x--;
	}
	template< class Arg>
	void find( bool (*eval)(const T&, const Arg&), const Arg &arg, int &x, int &y) const{
		y = -1;
		for( x=0; y == -1 && x < this->matrix.getLength(); x++) y = this->matrix[x].find<Arg>( eval, arg);
		x--;
	}
	////////////////////////erase
};

template< class T, int IncW = 30, int IncH = 30>
class Matrix: public MatrixBase< T, IncW, IncH>{
	Matrix( int W = 0, int H = 0, int StartW = IncW, int StartH = IncH):MatrixBase<T,IncW,IncH>( W, H, StartW, StartH){}
	Matrix( const T **src, int srcW, int srcH):MatrixBase<T,IncW,IncH>( src, srcW, srcH){}
	Matrix( const Matrix<T,IncW,IncH> &src):MatrixBase<T,IncW,IncH>(src){}
	using MatrixBase<T,IncW,IntH>::find;
	template< class Arg>
	void find( bool (T::*eval)(const Arg&), const Arg &arg, int &x, int &y) const{
		y = -1;
		for( x=0; y == -1 && x < this->matrix.getLength(); x++) y = this->matrix[x].find<Arg>( eval, arg)
	}
};

};

#endif//ANG_Matrix_HPP
