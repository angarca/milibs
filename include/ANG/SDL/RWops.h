#ifndef ANG_SDL_RWops_H
#define ANG_SDL_RWops_H

#include <SDL.h>
#include <ANG/String.h>

namespace ANG{
namespace SDL{

class RWops{
private:
	SDL_RWops *file;
	bool hasToClose, eofFlag;
	inline bool isAnyChar(const char c, const char *str);
public:
	RWops();
	RWops(SDL_RWops *src, bool HTC = false);
	RWops(const String &path, const char *mode = "r");
	~RWops();
	RWops &open(const String &path, const char *mode = "r");
	RWops &close();
	String readLine(const String &endLine = String("\n"));
	bool eof();
};

};};// namespace ANG::SDL

#endif//ANG_SDL_RWops_H
