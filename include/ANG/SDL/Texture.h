#ifndef ANG_SDL_Texture_H
#define ANG_SDL_Texture_H

#include <SDL.h>
#include <SDL_image.h>
#include <ANG/String.h>

namespace ANG{
namespace SDL{

class Texture{
protected:
	bool hasToDestroy;
	SDL_Renderer *renderer;
	SDL_Texture *texture;
	Sint32 W,H;
	inline void createTextureBase( const String &src);
	inline void destroyTextureBase();
public:
	Texture( SDL_Renderer *Renderer = nullptr, SDL_Texture *src = nullptr, bool HTD = false);
	Texture( SDL_Renderer *Renderer, const String &src);
	~Texture();
	Texture &setTexture( SDL_Texture *src, bool HTD = false);
	Texture &createTexture( const String &src);
	Texture &createTexture( SDL_Renderer *renderer, const String &src);
	Texture &destroyTexture();
	Texture &setRenderer( SDL_Renderer *renderer);
	Sint32 getW();
	Sint32 getH();
	bool getHasToDestroy();
	SDL_Texture *operator()();
};

};};

#endif//ANG_SDL_Texture_H
