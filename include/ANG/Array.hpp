#ifndef ANG_Array_HPP
#define ANG_Array_HPP

namespace ANG{

template< class T, int Inc = 30>
class ArrayBase{
public:
	enum OnCopy{ OVERWRITE, REBUILD, AUTOCUT};
protected:
	OnCopy onCopy;
	T *array;
	int length, maxLength;
	inline void resize(const int newLength){
		int copyLength = (newLength < this->maxLength) ? (newLength) : ( this->maxLength);
		T *tmp = array;
		array = new T[this->maxLength = newLength];
		this->copy( tmp, copyLength);
		delete[] tmp;
	}
	inline void copy(const T *src, int copyLength){
		for( int i=0; i < copyLength; i++) this->array[i] = src[i];
	}
	inline int findBase( const T &target) const{
		bool b = false; int i;
		for(i=0; !b && i < this->length; i++) b = this->array[i] == target;
/*		if( !b) throw "Error, ArrayBase::findBase: target not found.\n";*/
		return b ? --i : -1;	//--i;
	}
	template< class Arg>
	int findBase(bool (*eval)(const T&, const Arg&), const Arg &arg) const{
		bool b = false; int i;
		for(i=0; !b && i < this->length; i++)  b = eval( this->array[i], arg);
/*		if( !b) throw "Error, ArrayBase::findBase: arg not found with eval func.\n";*/
		return b ? --i : -1;	//--i;
	}
	inline void eraseBase( int index){
		this->length--;
		for( int i=index; i < this->length; i++) this->array[i] = this->array[i+1];
	}
public:
	ArrayBase(int size=0, int startMax=Inc, OnCopy OC=AUTOCUT):length(size),maxLength( size>startMax ? size : startMax ),onCopy(OC){
		if(this->maxLength == 0) this->maxLength = 1;
		this->array = new T[this->maxLength];
	}
	ArrayBase(const T *src, int srcLength, OnCopy OC=AUTOCUT):length(srcLength),maxLength(srcLength),onCopy(OC){
		if(this->maxLength == 0) this->maxLength = 1;
		this->array = new T[this->maxLength];
		this->copy( src, this->maxLength);
	}
	ArrayBase(const ArrayBase<T,Inc> &src, OnCopy OC=AUTOCUT):length(src.length),onCopy(OC){
		this->maxLength = this->onCopy == AUTOCUT ? this->length : src.maxLength;
		if(this->maxLength == 0) this->maxLength = 1;
		this->array = new T[this->maxLength];
		this->copy( src.array, this->maxLength);
	}
	~ArrayBase(){if( this->array) delete[] this->array;}
	ArrayBase<T,Inc> &setOnCopy( OnCopy onCopy){ this->onCopy = onCopy;}
	OnCopy getOnCopy() const { return this->onCopy;}
	ArrayBase<T,Inc> &operator=(const ArrayBase<T,Inc> &src){
		this->length = src.length;
		if( this->onCopy != OVERWRITE || this->length > this->maxLength){
			if( this->array) delete[] this->array;
			this->maxLength = this->onCopy == AUTOCUT ? this->length : src.maxLength;
			if(this->maxLength == 0) this->maxLength = 1;
			this->array = new T[this->maxLength];
		}
		this->copy( src.array, this->length);
		return *this;
	}
	bool operator==(const ArrayBase<T,Inc> &other) const {
		bool ret = this->length == other->length;
		for(int i=0; ret && i < this->length; i++)
			if(!( this->array[i] == other->array[i]))
				ret = false;
		return ret;
	}
	T &operator[](const int index) const {
/*		if(index < 0 || index >= this->length) throw "Error, ArrayBase::operator[]: \'index\' out of range.\n";*/
		return this->array[index];
	}
	T &operator()(const int index){// Equivalente a {setLength(i+1);operator[](i);}, ahorrando llamadas (pila), y comprobaciones repetidas.
/*		if(index < 0) throw "Error, ArrayBase::operator(): invalid \'index\' negative.\n";*/
		if(index >= this->length){ if(index >= this->maxLength) this->resize(index+1); this->length = index+1;}
		return this->array[index];
	}
	T *operator()() const { return array;}
	ArrayBase<T,Inc> &add(const T &newElem){
		if(this->length +1 > this->maxLength) this->resize(this->maxLength + Inc);
		array[this->length++] = newElem;
		return *this;
	}
	ArrayBase<T,Inc> &setLength(const int newLength){
/*		if(newLength < 0) throw "Error, ArrayBase::setLength: invalid \'newLength\' negative.\n";*/
		if(newLength >= this->maxLength) this->resize(newLength);
		this->length = newLength;
		return *this;
	}
	ArrayBase<T,Inc> &cutLength(){ if(this->length < this->maxLength) this->resize(this->length); return *this;}
	int getLength() const { return this->length; }
	bool contains(const T &elem) const {
		bool contain = false;
		for(int i=0; i < this->length; i++) if(array[i] == elem) contain = true;
		return contain;
	}
	int find( const T &target) const{
		return this->findBase(target);
	}
	template< class Arg>
	int find(bool (*eval)(const T&, const Arg&), const Arg &arg) const{
		return this->findBase<Arg>( eval, arg);
	}
	ArrayBase<T,Inc> &erase( int index){
		this->eraseBase( index);
		return *this;
	}
	ArrayBase<T,Inc> &erase( const T &target){
		this->eraseBase( this->findBase( target));
		return *this;
	}
	template< class Arg>
	ArrayBase<T,Inc> &erase( bool (*eval)(const T&, const Arg&), const Arg &arg){
		this->eraseBase( this->findBase<Arg>( eval, arg));
		return *this;
	}
};

template< class T, int Inc = 30>
class Array: public ArrayBase<T,Inc>{
protected:
	template< class Arg>
	inline int findBase(bool (T::*eval)(const Arg&) const, const Arg &arg) const{
		bool b = false; int i;
		for(i=0; !b && i < this->length; i++) b = (this->array[i].*eval)( arg);
/*		if( !b) throw Error, Array::findBase: arg not found with eval func.\n";*/
		return b ? --i : -1;	//--i;
	}
public:
	Array(const int size=0, const int startMax=Inc):ArrayBase<T,Inc>(size,startMax){}
	Array(const T *src, const int srcLength):ArrayBase<T,Inc>(src,srcLength){}
	Array(const Array<T,Inc> &src):ArrayBase<T,Inc>(src){}
	using ArrayBase<T,Inc>::find;
	template< class Arg>
	int find(bool (T::*eval)(const Arg&) const, const Arg &arg) const{
		return this->findBase<Arg>( eval, arg);
	}
	using ArrayBase<T,Inc>::erase;
	template< class Arg>
	Array<T,Inc> &erase( bool (T::*eval)(const Arg&) const, const Arg &arg){
		this->eraseBase( this->findBase<Arg>( eval, arg));
		return *this;
	}
};

};// namespace ANG

#endif//ANG_Array_HPP
