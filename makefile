.PHONY: nada clean cleanAll todo String RWops Texture Sprite

nada:

objetos = $(MIBIN)/String.o $(MIBIN)/RWops.o $(MIBIN)/Texture.o $(MIBIN)/Sprite.o
paquete = $(MILIB)/libmiLibs.a
ANGINCLUDE = $(MIINCLUDE)/ANG
ANGSRC = $(MISRC)/ANG
SDLINCLUDE = $(ANGINCLUDE)/SDL
SDLSRC = $(ANGSRC)/SDL
MIFLAGS = $(MILIBS)/flags

clean:
	rm -f $(paquete)

cleanAll: clean
	rm -f $(objetos)

todo: $(objetos)
	ar rsc $(paquete) $(objetos)

Sprite: $(MIFLAGS)/Sprite

$(MIFLAGS)/Sprite: $(MIBIN)/Sprite.o
	ar rsc $(paquete) $(MIBIN)/Sprite.o
	touch $(MIFLAGS)/Sprite

$(MIBIN)/Sprite.o: $(SDLSRC)/Sprite.cpp $(SDLINCLUDE)/Sprite.h
	g++ -c -std=c++11 -I$(MIINCLUDE) -I$(SDLINCLUDEPATH) $(SDLSRC)/Sprite.cpp -o $(MIBIN)/Sprite.o

$(SDLINCLUDE)/Sprite.h: $(SDLINCLUDE)/Texture.h $(ANGINCLUDE)/Matrix.hpp
	touch $(SDLINCLUDE)/Sprite.h

Texture: $(MIFLAGS)/Texture

$(MIFLAGS)/Texture: $(MIBIN)/Texture.o
	ar rsc $(paquete) $(MIBIN)/Texture.o
	touch $(MIFLAGS)/Texture

$(MIBIN)/Texture.o: $(SDLSRC)/Texture.cpp $(SDLINCLUDE)/Texture.h
	g++ -c -std=c++11 -I$(MIINCLUDE) -I$(SDLINCLUDEPATH) $(SDLSRC)/Texture.cpp -o $(MIBIN)/Texture.o

$(SDLINCLUDE)/Texture.h: $(ANGINCLUDE)/String.h
	touch $(SDLINCLUDE)/Texture.h

RWops: $(MIFLAGS)/RWops

$(MIFLAGS)/RWops: $(MIBIN)/RWops.o
	ar rsc $(paquete) $(MIBIN)/RWops.o
	touch $(MIFLAGS)/RWops

$(MIBIN)/RWops.o: $(SDLSRC)/RWops.cpp $(SDLINCLUDE)/RWops.h
	g++ -c -std=c++11 -I$(MIINCLUDE) -I$(SDLINCLUDEPATH) $(SDLSRC)/RWops.cpp -o $(MIBIN)/RWops.o

$(SDLINCLUDE)/RWops.h: $(ANGINCLUDE)/String.h
	touch $(SDLINCLUDE)/RWops.h

String: $(MIFLAGS)/String

$(MIFLAGS)/String: $(MIBIN)/String.o
	ar rsc $(paquete) $(MIBIN)/String.o
	touch $(MIFLAGS)/String

$(MIBIN)/String.o: $(ANGSRC)/String.cpp $(ANGINCLUDE)/String.h
	g++ -c -std=c++11 -I$(MIINCLUDE) $(ANGSRC)/String.cpp -o $(MIBIN)/String.o

$(ANGINCLUDE)/String.h: $(ANGINCLUDE)/Array.hpp
	touch $(ANGINCLUDE)/String.h
