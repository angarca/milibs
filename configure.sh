#!/bin/bash

if [ "$SDLPATH" = "" ]; then

echo '# SDL path' >> ~/.bashrc
echo export SDLPATH=/usr/local >> ~/.bashrc
echo export SDLINCLUDEPATH=/usr/local/include/SDL2 >> ~/.bashrc
echo export SDLLIBPATH=/usr/local/lib >> ~/.bashrc

fi

if [ "$MILIBS" = "" ]; then

MILIBS=$(pwd)
MIINCLUDE=$MILIBS/include
MILIB=$MILIBS/lib
MIBIN=$MILIBS/bin
MISRC=$MILIBS/src

echo '# miLibs' >> ~/.bashrc
echo export MILIBS=$MILIBS >> ~/.bashrc
echo export MIINCLUDE=$MIINCLUDE >> ~/.bashrc
echo export MILIB=$MILIB >> ~/.bashrc
echo export MIBIN=$MIBIN >> ~/.bashrc
echo export MISRC=$MISRC >> ~/.bashrc

fi

