#include <ANG/SDL/Texture.h>

using ANG::String;

// bool hasToDestroy;
// SDL_Renderer *renderer;
// SDL_Texture *texture;
// Sint32 W,H;

void ANG::SDL::Texture::createTextureBase( const String &src){
	SDL_Surface *img = IMG_Load( src());
	this->W = img->w;	this->H = img->h;
	this->texture = SDL_CreateTextureFromSurface( this->renderer, img);
	SDL_FreeSurface( img);
	this->hasToDestroy = true;
}

void ANG::SDL::Texture::destroyTextureBase(){
	if( this->hasToDestroy && this->texture) SDL_DestroyTexture( this->texture);
	this->texture = nullptr;	this->hasToDestroy = false;
}

ANG::SDL::Texture::Texture( SDL_Renderer *Renderer, SDL_Texture *src, bool HTD):renderer(Renderer),texture(src),hasToDestroy(HTD){}

ANG::SDL::Texture::Texture( SDL_Renderer *Renderer, const String &src):renderer(Renderer){
	this->createTextureBase( src);
}

ANG::SDL::Texture::~Texture(){
	if( this->hasToDestroy && this->texture) SDL_DestroyTexture( this->texture);
}

ANG::SDL::Texture &ANG::SDL::Texture::setTexture( SDL_Texture *src, bool HTD){
	if( this->hasToDestroy && this->texture) SDL_DestroyTexture( this->texture);
	this->texture = src;	this->hasToDestroy = HTD;
	return *this;
}

ANG::SDL::Texture &ANG::SDL::Texture::createTexture( const String &src){
	if(! this->renderer);	// throw "Error, ANG::SDL::Texture::createTexture: this->renderer = nullptr";
	if( this->hasToDestroy && this->texture) SDL_DestroyTexture( this->texture);
	this->createTextureBase( src);
	return *this;
}

ANG::SDL::Texture &ANG::SDL::Texture::createTexture( SDL_Renderer *renderer, const String &src){
	this->renderer = renderer;
	if( this->hasToDestroy && this->texture) SDL_DestroyTexture( this->texture);
	this->createTextureBase( src);
	return *this;
}

ANG::SDL::Texture &ANG::SDL::Texture::destroyTexture(){
	this->destroyTextureBase();
	return *this;
}

ANG::SDL::Texture &ANG::SDL::Texture::setRenderer( SDL_Renderer *renderer){
	this->destroyTextureBase();
	this->renderer = renderer;
	return *this;
}

Sint32 ANG::SDL::Texture::getW(){ return this->W;}
Sint32 ANG::SDL::Texture::getH(){ return this->H;}
bool ANG::SDL::Texture::getHasToDestroy(){ return this->hasToDestroy;}
SDL_Texture *ANG::SDL::Texture::operator()(){ return this->texture;}

