#include <ANG/SDL/RWops.h>

using ANG::String;

// SDL_RWops * this->file
// bool hasToClose, eofFlag

ANG::SDL::RWops::RWops():file(nullptr),hasToClose(false),eofFlag(false){}

ANG::SDL::RWops::RWops(SDL_RWops *src, bool HTC):file(src),hasToClose(HTC),eofFlag(false){}

ANG::SDL::RWops::RWops(const String &path, const char *mode):hasToClose(true),eofFlag(false){
	this->file = SDL_RWFromFile( path(), mode);
}

ANG::SDL::RWops::~RWops(){
	if(this->hasToClose && this->file)	SDL_RWclose(this->file);
}

ANG::SDL::RWops &ANG::SDL::RWops::open(const String &path, const char *mode){
	if(this->hasToClose && this->file)	SDL_RWclose(this->file);
	this->file = SDL_RWFromFile( path(), mode);
	hasToClose = true;	eofFlag = false;
}

ANG::SDL::RWops &ANG::SDL::RWops::close(){
	if(this->file) SDL_RWclose(this->file);
	hasToClose = false;	eofFlag = false;
}


String ANG::SDL::RWops::readLine(const String &endLine){
	String line;	char tmp;
	SDL_RWread(file, &tmp, sizeof(char), 1);
	while(!this->eofFlag && !this->isAnyChar(tmp,endLine())){
		line += tmp;
		if(SDL_RWread(file, &tmp, sizeof(char), 1) <= 0) this->eofFlag = true;
	}
	return line;
}

bool ANG::SDL::RWops::isAnyChar(const char c, const char *str){
	bool b = false;
	for( Sint32 i=0; str[i]!='\0'; i++) if( str[i]==c) b = true;
	return b;
}

bool ANG::SDL::RWops::eof(){ return this->eofFlag;}
