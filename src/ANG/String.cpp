#include <ANG/String.h>

int ANG::String::count(const char *str, const char endChar) const{
	int i;	for( i=0; str[i] != endChar; i++);
	return i;
}

ANG::String::String(){
	this->setLength(1);
	this->array[0] = '\0';
};

ANG::String::String(const char c){
	this->setLength(2);
	this->array[0] = c;
	this->array[1] = '\0';
}

ANG::String::String(const char *src){
	int i, size = this->count(src);
	this->setLength(++size);
	for(i=0; i < size; i++) array[i] = src[i];
}

ANG::String::String(const ANG::String &src){(*this) = src;}

ANG::String &ANG::String::operator+=(const char c){
	return this->operator+=(String(c));
}

ANG::String &ANG::String::operator+=(const char *str){
	return this->operator+=(String(str));
}

ANG::String &ANG::String::operator+=(const ANG::String &other){
	int i, size = this->length-1;
	this->setLength(size + other.length);
	for( i=0; i < other.length; i++) this->array[i+size] = other.array[i];
	return *this;
}

ANG::String ANG::String::operator+(const char c) const{
	return this->operator+(String(c));
}

ANG::String ANG::String::operator+(const char *str) const{
	return this->operator+(String(str));
}

ANG::String ANG::String::operator+(const ANG::String &other) const{
	int i, size = this->length-1;	String result(*this);
	result.setLength(size + other.length);
	for( i=0; i < other.length; i++) result.array[i+size] = other.array[i];
	return result;
}

bool ANG::String::operator==(const char c) const { return this->operator==(String(c)); }

bool ANG::String::operator==(const char *str) const { return this->operator==(String(str)); }

bool ANG::String::operator==(const String &other) const {
	bool ret = this->length == other.length;
	for(int i=0; ret && i < this->length; i++)
		if( this->array[i] != other.array[i])
			ret = false;
	return ret;
}

bool ANG::String::isEmpty() const { return this->length <= 1;}

int ANG::String::to_i() const {
	int i, ret = 0;
	for( i=0; this->array[i] >= '0' && this->array[i] <= '9' && i < this->length; i++){
		ret *= 10;
		ret += this->array[i] - '0';
	}
	return ret;
}

ANG::Array<ANG::String> ANG::String::split(const ANG::String &endChars) const {
	ANG::Array<ANG::String> strings;
	ANG::String tmp;
	for(int i=0; i < this->length; i++){
		if( endChars.contains( this->array[i])){
			if(!tmp.isEmpty())
				strings.add(tmp);
			tmp = String("");
		}else{
			tmp += this->array[i];
		}
	}
	if(!tmp.isEmpty())
		strings.add(tmp);
	return strings;
}
