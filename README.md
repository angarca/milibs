# README #

This is a simple auxiliary library to be used in other projects.

To use it, first you need to have installed http://libsdl.org/download-2.0.php under: /usr/local/include/SDL2 /usr/local/lib

Once downloaded to make it visible to the other projects, you need to set up some environment variables, to do this in bashrc, just run ./configure.sh
